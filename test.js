const express = require('express')
var bodyParser = require('body-parser');
const  route_creation = require('./routes/routes');
const  route_login = require('./routes/loginrouting');
var cors = require('cors')



var app = express();
app.use(cors())

require('dotenv').config();
app.listen(process.env.server_port)
app.use(bodyParser.json())
app.use(
    bodyParser.urlencoded({
        extended: true,
    }))

    
app.use('/',route_creation)
app.use('/',route_login)



console.log("listen on  " + process.env.server_port)



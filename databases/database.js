var pg = require('pg');
require('dotenv').config();


var config = {
    user: process.env.user,
    host: process.env.host,
    database: process.env.database,
    port: process.env.port
};
var pool = new pg.Pool(config);
pool.connect(function (err, client, done) {
    





    
    if (err) {
        console.error('could not connect to cockroachdb', err);
        finish();
    }


    return client;
  
});

module.exports = {
    pool
  }